package edu.wichita.anothergenericweatherapp.dialog;

import android.app.AlertDialog;
import android.content.DialogInterface;

import edu.wichita.anothergenericweatherapp.sharing.ShareStrategy;

/**
 * Created by Osama Rao on 03-Dec-15.
 */
public class ShareDialog implements DialogInterface.OnClickListener {

    private String[] shareServices;
    private ShareStrategy[] shareStrategies;
    private ShareStrategy.ShareArguments mShareArguments;


    public AlertDialog getShareDialog(ShareStrategy[] shareStrategies, ShareStrategy.ShareArguments shareArguments) {
        this.shareStrategies = shareStrategies;
        mShareArguments = shareArguments;
        shareServices = new String[shareStrategies.length];

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mShareArguments.activity);
        for (int i = 0; i < shareServices.length; i++) {
            shareServices[i] = shareStrategies[i].getDisplayName();
        }

        alertDialogBuilder.setItems(shareServices, this);
        return alertDialogBuilder.create();

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        shareStrategies[which].shareWeather(mShareArguments);
        dialog.dismiss();
    }
}
