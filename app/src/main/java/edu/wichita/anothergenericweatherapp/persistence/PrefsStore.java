package edu.wichita.anothergenericweatherapp.persistence;

import android.content.SharedPreferences;

import javax.inject.Inject;

public class PrefsStore {

    private final String KEY_CITIES_SAVED = "KEY_CITIES_SAVED";
    private SharedPreferences preferences;

    @Inject
    public PrefsStore(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public boolean areCitiesSaved() {
        return preferences.getBoolean(KEY_CITIES_SAVED, false);
    }

    public void setCitiesSaved() {
        preferences.edit().putBoolean(KEY_CITIES_SAVED, true).apply();
    }
}