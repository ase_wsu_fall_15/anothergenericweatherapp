package edu.wichita.anothergenericweatherapp.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Random;

import javax.inject.Inject;

import androidx.core.app.NotificationCompat;
import edu.wichita.anothergenericweatherapp.AnotherGenericWeatherApplication;
import edu.wichita.anothergenericweatherapp.R;
import edu.wichita.anothergenericweatherapp.api.ForecastIO;
import edu.wichita.anothergenericweatherapp.models.forecastio.Weather;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Osama Rao on 30-Nov-15.
 */
public class WeatherNotificationService extends IntentService {
    @Inject
    ForecastIO forecastIO;

    public WeatherNotificationService() {
        super("");

    }

    public WeatherNotificationService(String name) {
        super("WeatherNotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        startPolling(this);
        ((AnotherGenericWeatherApplication) getApplication()).getAppComponent().inject(this);
    }

    public void startPolling(final Context context) {
        forecastIO.getWeatherFromLatLng(37.697948, -97.314835).enqueue(new Callback<Weather>() {
            @Override
            public void onResponse(Response<Weather> response, Retrofit retrofit) {
                showNotification(context, response.body());
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void showNotification(Context context, Weather weather) {
        Log.d("showNotification", "IN!");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // Create Notification
        Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("AGWA")
                .setContentText("Current Temperature in Wichita, KS " + weather.getCurrently().getTemperature())
                .build();
        notificationManager.notify(new Random().nextInt(), notification);

    }
}
