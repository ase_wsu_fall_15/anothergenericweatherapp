package edu.wichita.anothergenericweatherapp.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

import androidx.legacy.content.WakefulBroadcastReceiver;

/**
 * Created by Osama Rao on 30-Nov-15.
 */
public class WeatherBroadcastReceiver extends WakefulBroadcastReceiver {
    private AlarmManager alarmMgr;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("onReceive", "IN!");
        Intent service = new Intent(context, WeatherNotificationService.class);

        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, service);
    }

    public void setAlarm(Context context, int intervalInMinutes){
        Log.d("setAlarm", "IN!");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, WeatherBroadcastReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), intervalInMinutes * 60 * 60 * 1000, alarmIntent);

    }

}
