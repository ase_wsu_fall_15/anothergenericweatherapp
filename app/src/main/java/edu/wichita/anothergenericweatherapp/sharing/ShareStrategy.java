package edu.wichita.anothergenericweatherapp.sharing;

import android.app.Activity;

import edu.wichita.anothergenericweatherapp.models.forecastio.Weather;
import edu.wichita.anothergenericweatherapp.util.WeatherFormatter;

/**
 * Created by Osama Rao on 03-Dec-15.
 */
public interface ShareStrategy {

    void shareWeather(ShareArguments shareArguments);

    String getDisplayName();

    class ShareArguments {

        public final Activity activity;
        private final Weather weather;
        private final String userCity;

        public ShareArguments(Activity activity, Weather weather, String userCity) {
            this.activity = activity;
            this.weather = weather;
            this.userCity = userCity;
        }

        public String getShareMessage() {
            return new WeatherFormatter(this.activity.getResources()).prepareShareWeatherText(this.userCity, this.weather);
        }
    }
}
