package edu.wichita.anothergenericweatherapp.sharing;

import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Osama Rao on 03-Dec-15.
 */
public class ShareViaEmail implements ShareStrategy {

    @Override
    public void shareWeather(ShareArguments shareArguments) {
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setType("message/rfc822");
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Weather where I am!");
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareArguments.getShareMessage());

        try {
            shareArguments.activity.startActivity(Intent.createChooser(sendIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(shareArguments.activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public String getDisplayName() {
        return "Email";
    }
}
