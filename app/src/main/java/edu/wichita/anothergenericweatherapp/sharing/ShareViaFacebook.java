package edu.wichita.anothergenericweatherapp.sharing;

import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Osama Rao on 03-Dec-15.
 */
public class ShareViaFacebook implements ShareStrategy {

    @Override
    public void shareWeather(ShareArguments shareArguments) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareArguments.getShareMessage());
        sendIntent.setType("text/plain");
        sendIntent.setPackage("com.facebook.orca");

        try {
            shareArguments.activity.startActivity(sendIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(shareArguments.activity, "Please Install Facebook Messenger", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public String getDisplayName() {
        return "Facebook";
    }
}