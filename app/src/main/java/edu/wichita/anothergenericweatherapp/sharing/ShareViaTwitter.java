package edu.wichita.anothergenericweatherapp.sharing;

import android.content.Intent;
import android.net.Uri;

/**
 * Created by Osama Rao on 03-Dec-15.
 */
public class ShareViaTwitter implements ShareStrategy {
    @Override
    public void shareWeather(ShareArguments shareArguments) {
        String tweetUrl = String.format("https://twitter.com/intent/tweet?text=%s", Uri.encode(shareArguments.getShareMessage()));
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(tweetUrl.replace(" ", "%20")));
        shareArguments.activity.startActivity(i);
    }

    @Override
    public String getDisplayName() {
        return "Twitter";
    }
}