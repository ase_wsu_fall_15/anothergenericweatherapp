package edu.wichita.anothergenericweatherapp.data;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import javax.inject.Inject;

import edu.wichita.anothergenericweatherapp.models.Location;
import edu.wichita.anothergenericweatherapp.models.RealmLocation;
import edu.wichita.anothergenericweatherapp.models.UserLocation;
import edu.wichita.anothergenericweatherapp.persistence.PrefsStore;
import edu.wichita.anothergenericweatherapp.util.LocationsUtil;
import io.realm.Realm;
import timber.log.Timber;

public class LocationRepository {
    private Realm realm;
    private PrefsStore prefsStore;
    private Context context;
    private OnCitiesSavedListener onCitiesSavedListener;
    private OnLocationsUpdated onLocationsUpdated;

    @Inject
    public LocationRepository(Realm realm, PrefsStore prefsStore, Context context) {
        this.realm = realm;
        this.prefsStore = prefsStore;
        this.context = context;
    }

    public void setOnCitiesSavedListener(OnCitiesSavedListener listener) {
        this.onCitiesSavedListener = listener;
    }

    public void setOnLocationsUpdated(OnLocationsUpdated onLocationsUpdated) {
        this.onLocationsUpdated = onLocationsUpdated;
    }

    public Location getRealmLocationForZipCode(Integer zipCode) {
        //noinspection ConstantConditions
        return new Location(realm.where(RealmLocation.class).equalTo("zip", zipCode).findFirst());
    }

    public ArrayList<Location> getAllCities() {
        return (ArrayList<Location>) LocationsUtil.ToLocationsFromRealmLocations(realm.where(RealmLocation.class).findAll());
    }

    public ArrayList<Location> getAllSavedLocations() {
        return (ArrayList<Location>) LocationsUtil.ToLocationsFromUserLocations(realm.where(UserLocation.class).findAll());
    }

    public void createAll(List<Location> locations) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(LocationsUtil.ToRealmLocations(locations));
        realm.commitTransaction();
        onCitiesSavedListener.onCitiesSavedLoading(false);
    }

    public void delete(Integer zipCode) {
        realm.beginTransaction();
        RealmLocation record = realm.where(RealmLocation.class).equalTo("zip", zipCode).findFirst();
//        record.removeFromRealm();
        realm.commitTransaction();
    }


    public void persistAllPossibleLocations() {
        if (!prefsStore.areCitiesSaved()) {
            new PersistLocationsAsyncTask().execute();
        } else {
            onCitiesSavedListener.onCitiesSavedLoading(false);
        }
    }

    public void save(final Location location) {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            UserLocation userLocation = realm.createObject(UserLocation.class);

            userLocation.setCity(location.getCity());
            userLocation.setState(location.getState());
            userLocation.setLat(location.getLat());
            userLocation.setLng(location.getLng());
            userLocation.setZip(location.getZip());

            realm.copyToRealm(userLocation);
        });
        onLocationsUpdated.onLocationsUpdated();
    }

    public interface OnCitiesSavedListener {
        void onCitiesSavedLoading(boolean loading);
    }

    public interface OnLocationsUpdated {
        void onLocationsUpdated();
    }

    public class PersistLocationsAsyncTask extends AsyncTask<Void, Void, ArrayList<Location>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            onCitiesSavedListener.onCitiesSavedLoading(true);
        }

        @Override
        protected ArrayList<Location> doInBackground(final Void... voids) {
            InputStream is;
            Timber.d("doInBackground here");

            try {
                is = context.getAssets().open("cities.json");
                prefsStore.setCitiesSaved();
            } catch (IOException e) {
                e.printStackTrace();
                return new ArrayList<>();
            }

            // Accumulate the json in a string
            Scanner scanner = new Scanner(Objects.requireNonNull(is), Charset.defaultCharset().name());
            StringBuilder stringBuilder = new StringBuilder();
            while (scanner.hasNextLine()) {
                stringBuilder.append(scanner.nextLine());
            }
            String contentAsJson = stringBuilder.toString();

            // Create objects from the file
            Type listType = new TypeToken<ArrayList<Location>>() {
            }.getType();
            return new Gson().fromJson(contentAsJson, listType);
        }

        @Override
        protected void onPostExecute(final ArrayList<Location> locations) {
            super.onPostExecute(locations);
            onCitiesSavedListener.onCitiesSavedLoading(false);
            createAll(locations);
        }
    }

}
