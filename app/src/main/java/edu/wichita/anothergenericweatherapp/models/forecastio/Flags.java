
package edu.wichita.anothergenericweatherapp.models.forecastio;

import java.util.ArrayList;
import java.util.List;

public class Flags {

    private List<String> sources = new ArrayList<String>();
    private String units;

    /**
     *
     * @return
     *     The sources
     */
    public List<String> getSources() {
        return sources;
    }

    /**
     *
     * @param sources
     *     The sources
     */
    public void setSources(List<String> sources) {
        this.sources = sources;
    }

    /**
     *
     * @return
     *     The units
     */
    public String getUnits() {
        return units;
    }

    /**
     *
     * @param units
     *     The units
     */
    public void setUnits(String units) {
        this.units = units;
    }

}
