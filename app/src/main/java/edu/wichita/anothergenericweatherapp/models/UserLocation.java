package edu.wichita.anothergenericweatherapp.models;

import io.realm.RealmObject;

/**
 * Upon clicking on a location from the autocomplete, the selected location is saved as a UserLocation
 */
public class UserLocation extends RealmObject {


    private Integer zip;
    private String state;
    private String city;
    private Double lat;
    private Double lng;

    public UserLocation() {

    }

    public UserLocation(Location location) {
        this.zip = location.getZip();
        this.city = location.getCity();
        this.state = location.getState();
        this.lat = location.getLat();
        this.lng = location.getLng();
    }

    /**
     * @return The zip
     */
    public Integer getZip() {
        return zip;
    }

    /**
     * @param zip The zip
     */
    public void setZip(Integer zip) {
        this.zip = zip;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The lat
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The lng
     */
    public Double getLng() {
        return lng;
    }

    /**
     * @param lng The lng
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }
}
