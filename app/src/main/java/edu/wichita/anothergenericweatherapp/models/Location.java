package edu.wichita.anothergenericweatherapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import edu.wichita.anothergenericweatherapp.models.forecastio.Weather;

public final class Location implements Comparable<Location> {

    @SerializedName("zip")
    private Integer zip;
    @SerializedName("state")
    private String state;
    @SerializedName("city")
    private String city;
    @SerializedName("lat")
    private Double lat;
    @SerializedName("lng")
    private Double lng;
    private Weather weather;

    public Location() {

    }

    public Location(RealmLocation realmLocation) {
        this.zip = realmLocation.getZip();
        this.city = realmLocation.getCity();
        this.state = realmLocation.getState();
        this.lat = realmLocation.getLat();
        this.lng = realmLocation.getLng();
    }

    public Location(UserLocation userLocation) {
        this.zip = userLocation.getZip();
        this.city = userLocation.getCity();
        this.state = userLocation.getState();
        this.lat = userLocation.getLat();
        this.lng = userLocation.getLng();
    }

    /**
     * @return The zip
     */
    public Integer getZip() {
        return zip;
    }

    /**
     * @param zip The zip
     */
    public void setZip(Integer zip) {
        this.zip = zip;
    }

    /**
     * @return The state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The latitude
     */
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat The latitude
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return The longitude
     */
    public Double getLng() {
        return lng;
    }

    /**
     * @param lng The longitude
     */
    public void setLng(Double lng) {
        this.lng = lng;
    }


    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Location location = (Location) o;
        return Objects.equals(zip, location.zip) &&
                Objects.equals(state, location.state) &&
                Objects.equals(city, location.city) &&
                Objects.equals(lat, location.lat) &&
                Objects.equals(lng, location.lng) &&
                Objects.equals(weather, location.weather);
    }

    @Override
    public int hashCode() {

        return Objects.hash(zip, state, city, lat, lng, weather);
    }

    @Override
    public int compareTo(final Location that) {
        return this.getCity().compareTo(that.getCity());
    }
}
