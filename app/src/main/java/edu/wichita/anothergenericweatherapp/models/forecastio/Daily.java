
package edu.wichita.anothergenericweatherapp.models.forecastio;

import java.util.ArrayList;
import java.util.List;
public class Daily {

    private String summary;
    private String icon;
    private List<Datum_> data = new ArrayList<Datum_>();

    /**
     *
     * @return
     *     The summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     *
     * @param summary
     *     The summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     *
     * @return
     *     The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     *
     * @param icon
     *     The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     *
     * @return
     *     The data
     */
    public List<Datum_> getData() {
        return data;
    }

    /**
     *
     * @param data
     *     The data
     */
    public void setData(List<Datum_> data) {
        this.data = data;
    }

}
