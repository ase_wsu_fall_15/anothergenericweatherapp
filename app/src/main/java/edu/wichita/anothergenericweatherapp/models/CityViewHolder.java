package edu.wichita.anothergenericweatherapp.models;

import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import edu.wichita.anothergenericweatherapp.R;
import edu.wichita.anothergenericweatherapp.WeatherActivity;
import edu.wichita.anothergenericweatherapp.adapters.ViewPresenter;

public class CityViewHolder extends ViewPresenter<Location> implements View.OnClickListener {
    private View cityViewItem;
    private TextView name;
    private TextView temperature;
    private Location city;

    public CityViewHolder(View itemView) {
        super(itemView);
        name = itemView.findViewById(R.id.cityName);
        cityViewItem = itemView.findViewById(R.id.cityViewItem);
        temperature = itemView.findViewById(R.id.cityTemperature);
    }

    @Override
    public void bind(final Location city) {
        this.city = city;
        name.setText(itemView.getResources().getString(R.string.city_name_format,  city.getCity(), city.getState(), city.getZip()));
        if (city.getWeather() != null)
            temperature.setText(String.format(Locale.getDefault(), "%1$d F", city.getWeather().getCurrently().getTemperature().intValue()));
        cityViewItem.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        v.getContext().startActivity(WeatherActivity.newIntent(v.getContext(), city));
    }
}
