package edu.wichita.anothergenericweatherapp;

import android.app.Application;

import edu.wichita.anothergenericweatherapp.dagger.AppComponent;
import edu.wichita.anothergenericweatherapp.dagger.ApplicationModule;
import edu.wichita.anothergenericweatherapp.dagger.DaggerAppComponent;
import io.realm.Realm;
import timber.log.Timber;


public class AnotherGenericWeatherApplication extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        Timber.plant(new Timber.DebugTree());


        Realm.init(this);
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }
}
