package edu.wichita.anothergenericweatherapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import edu.wichita.anothergenericweatherapp.api.ForecastIO;
import edu.wichita.anothergenericweatherapp.models.Location;
import edu.wichita.anothergenericweatherapp.models.forecastio.Datum;
import edu.wichita.anothergenericweatherapp.models.forecastio.Datum_;
import edu.wichita.anothergenericweatherapp.models.forecastio.Weather;
import edu.wichita.anothergenericweatherapp.sharing.ShareStrategy;
import edu.wichita.anothergenericweatherapp.sharing.ShareViaEmail;
import edu.wichita.anothergenericweatherapp.sharing.ShareViaFacebook;
import edu.wichita.anothergenericweatherapp.sharing.ShareViaTwitter;
import edu.wichita.anothergenericweatherapp.util.DialogFactory;


public class WeatherActivity extends AppCompatActivity {
    private static final String KEY_LOCATION_CITY_NAME = "LOCATION_CITY_NAME";
    private static final String KEY_LOCATION_STATE = "LOCATION_STATE";
    private static final String KEY_LOCATION_ZIP = "LOCATION_ZIP";
    private static final String KEY_LOCATION_LATITUDE = "LOCATION_LATITUDE";
    private static final String KEY_LOCATION_LONGITUDE = "LOCATION_LONGITUDE";
    FloatingActionButton fab;
    Weather weather;
    @Inject
    ForecastIO forecastIO;
    private TextView txtTemp;
    private TextView txtHumidity;
    private TextView txtWindSpeed;
    private TextView txtWindBearing;
    private TextView txtPrecipitationProbability;
    private TextView txtMinimumTemperature;
    private TextView txtMaximumTemperature;
    private TextView txtTime;
    private String cityName;
    private TextView icon;

    public static Intent newIntent(Context ctx, Location location) {
        Intent intent = new Intent(ctx, WeatherActivity.class);
        intent.putExtra(KEY_LOCATION_CITY_NAME, location.getCity());
        intent.putExtra(KEY_LOCATION_STATE, location.getState());
        intent.putExtra(KEY_LOCATION_ZIP, location.getZip());
        intent.putExtra(KEY_LOCATION_LATITUDE, location.getLat().doubleValue());
        intent.putExtra(KEY_LOCATION_LONGITUDE, location.getLng().doubleValue());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        ((AnotherGenericWeatherApplication) getApplication()).getAppComponent().inject(this);
        fab = findViewById(R.id.fab);
        txtTime = findViewById(R.id.txtTime);
        txtTemp = findViewById(R.id.txtTemp);
        txtHumidity = findViewById(R.id.txtHumidity);
        txtWindSpeed = findViewById(R.id.txtWindSpeed);
        txtWindBearing = findViewById(R.id.txtWindBearing);
        txtPrecipitationProbability = findViewById(R.id.txtPrecipitationProbability);
        txtMinimumTemperature = findViewById(R.id.txtMinimumTemperature);
        txtMaximumTemperature = findViewById(R.id.txtMaximumTemperature);
        icon = findViewById(R.id.icon);

        fab.setOnClickListener(v -> {
            ShareStrategy.ShareArguments shareArguments = new ShareStrategy.ShareArguments(WeatherActivity.this, weather, cityName);
            ShareStrategy[] shareStrategies = new ShareStrategy[]{new ShareViaFacebook(), new ShareViaTwitter(), new ShareViaEmail()};
            AlertDialog weatherShareDialog = DialogFactory.getWeatherShareDialog(shareStrategies, shareArguments);
            weatherShareDialog.show();
        });

        // Get data from launcher intent
        Bundle extras = Objects.requireNonNull(getIntent().getExtras());
        Double latitude = extras.getDouble(KEY_LOCATION_LATITUDE);
        Double longitude = extras.getDouble(KEY_LOCATION_LONGITUDE);

        cityName = getString(R.string.city_name_format, extras.getString(KEY_LOCATION_CITY_NAME), extras.getString(KEY_LOCATION_STATE), extras.getInt(KEY_LOCATION_ZIP));
        setTitle(cityName);
        txtTime.setText(SimpleDateFormat.getTimeInstance().format(new Date()));
        forecastIO.getWeatherFromLatLng(latitude, longitude).enqueue(new retrofit.Callback<Weather>() {
            @Override
            public void onResponse(final retrofit.Response<Weather> response, final retrofit.Retrofit retrofit) {
                weather = response.body();
                setUpUI(weather);
            }

            @Override
            public void onFailure(final Throwable t) {
                Toast.makeText(WeatherActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpUI(Weather weather) {
        Datum_ daily = weather.getDaily().getData().get(0);
        Datum hourly = weather.getHourly().getData().get(0);

        txtHumidity.setText(hourly.getHumidity());
        txtTemp.setText(getString(R.string.temperature_farenheit, hourly.getTemperature()));
        txtWindSpeed.setText(getString(R.string.windspeed_mph, hourly.getWindSpeed()));
        txtWindBearing.setText(getString(R.string.windbearing_angle, hourly.getWindBearing() % 180d));
        txtPrecipitationProbability.setText(getString(R.string.precipitation_prob_value, hourly.getPrecipProbability()));
        txtMinimumTemperature.setText(getString(R.string.temperature_farenheit, daily.getTemperatureMin()));
        txtMaximumTemperature.setText(getString(R.string.temperature_farenheit, daily.getTemperatureMax()));
        icon.setText(hourly.getIcon());
    }


}
