package edu.wichita.anothergenericweatherapp;

import java.util.ArrayList;

import edu.wichita.anothergenericweatherapp.models.Location;

interface MainActivityViewModel {

    ArrayList<Location> savedLocations();

    ArrayList<Location> allCities();

    void saveLocationToDatabase(Location location);
}
