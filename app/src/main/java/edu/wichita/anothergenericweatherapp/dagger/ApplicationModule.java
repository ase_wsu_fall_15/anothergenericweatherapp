package edu.wichita.anothergenericweatherapp.dagger;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import edu.wichita.anothergenericweatherapp.R;
import io.realm.Realm;

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    public Resources provideResources(Context context){
        return context.getResources();
    }

    @Provides
    @Singleton
    public Realm provideRealm(){
        return Realm.getDefaultInstance();
    }

}
