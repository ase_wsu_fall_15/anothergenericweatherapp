package edu.wichita.anothergenericweatherapp.dagger;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import edu.wichita.anothergenericweatherapp.api.ForecastIO;
import edu.wichita.anothergenericweatherapp.api.WebServiceConstants;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

@Module
public class ApiModule {

    @Provides
    @Singleton
    public HttpLoggingInterceptor providesHttpLoggingInterceptor(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides
    @Singleton
    public OkHttpClient providesOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor){
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(httpLoggingInterceptor);
        return client;
    }


    @Provides
    @Singleton
    public Retrofit providesRetrofit(OkHttpClient client){
        return  new Retrofit.Builder()
                .baseUrl(WebServiceConstants.LIVE_SERVER)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    public ForecastIO provideForecastIO(Retrofit retrofit){
        return retrofit.create(ForecastIO.class);
    }
}
