package edu.wichita.anothergenericweatherapp.dagger;

import javax.inject.Singleton;

import dagger.Component;
import edu.wichita.anothergenericweatherapp.AnotherGenericWeatherApplication;
import edu.wichita.anothergenericweatherapp.MainActivity;
import edu.wichita.anothergenericweatherapp.WeatherActivity;
import edu.wichita.anothergenericweatherapp.services.WeatherNotificationService;

@Component(modules = {ApplicationModule.class, ApiModule.class})
@Singleton
public interface AppComponent {

    void inject(AnotherGenericWeatherApplication anotherGenericWeatherApplication);

    void inject(WeatherActivity activity);

    void inject(MainActivity activity);

    void inject(WeatherNotificationService activity);
}
