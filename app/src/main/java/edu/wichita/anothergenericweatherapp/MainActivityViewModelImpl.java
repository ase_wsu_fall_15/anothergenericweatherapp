package edu.wichita.anothergenericweatherapp;

import java.util.ArrayList;

import javax.inject.Inject;

import edu.wichita.anothergenericweatherapp.data.LocationRepository;
import edu.wichita.anothergenericweatherapp.models.Location;

public class MainActivityViewModelImpl implements MainActivityViewModel, LocationRepository.OnCitiesSavedListener {

    private LocationRepository locationRepository;
    private OnLoadingFinishedListener onLoadingFinishedListener;

    @Inject
    public MainActivityViewModelImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
        locationRepository.setOnCitiesSavedListener(this);
    }

    public void persistLocations(){
        locationRepository.persistAllPossibleLocations();
    }

    public void setOnLoadingFinishedListener(OnLoadingFinishedListener onLoadingFinishedListener) {
        this.onLoadingFinishedListener = onLoadingFinishedListener;
    }

    public void setOnLocationsUpdated(LocationRepository.OnLocationsUpdated onLocationsUpdated) {
        locationRepository.setOnLocationsUpdated(onLocationsUpdated);
    }

    @Override
    public ArrayList<Location> savedLocations() {
        return locationRepository.getAllSavedLocations();
    }

    @Override
    public ArrayList<Location> allCities() {
        return locationRepository.getAllCities();
    }

    @Override
    public void saveLocationToDatabase(final Location location) {
        locationRepository.save(location);
    }

    @Override
    public void onCitiesSavedLoading(final boolean loading) {
        if (onLoadingFinishedListener != null) {
            onLoadingFinishedListener.onLoadingFinished(loading);
        }
    }

    public interface OnLoadingFinishedListener {
        void onLoadingFinished(boolean isLoading);
    }
}
