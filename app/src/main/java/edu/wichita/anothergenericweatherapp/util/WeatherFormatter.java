package edu.wichita.anothergenericweatherapp.util;

import android.content.res.Resources;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import edu.wichita.anothergenericweatherapp.R;
import edu.wichita.anothergenericweatherapp.models.forecastio.Datum;
import edu.wichita.anothergenericweatherapp.models.forecastio.Datum_;
import edu.wichita.anothergenericweatherapp.models.forecastio.Weather;

public class WeatherFormatter {

    private final Resources resources;

    public WeatherFormatter(Resources resources) {
        this.resources = resources;
    }

    public String prepareShareWeatherText(String userCity, Weather weather) {
        Datum_ daily = weather.getDaily().getData().get(0);
        Datum hourly = weather.getHourly().getData().get(0);
        List<String> shareWeatherMessageParts = new ArrayList<>();


        shareWeatherMessageParts.add("Weather For : " + userCity);
        shareWeatherMessageParts.add("Temperature: " + this.resources.getString(R.string.temperature_farenheit, hourly.getTemperature()));
        shareWeatherMessageParts.add("Wind Speed: " + this.resources.getString(R.string.windspeed_mph, hourly.getWindSpeed()));
        shareWeatherMessageParts.add("Wind Bearing: " + this.resources.getString(R.string.windbearing_angle, hourly.getWindBearing() % 180d));
        shareWeatherMessageParts.add("Precipitation Probability: " + this.resources.getString(R.string.precipitation_prob_value, hourly.getPrecipProbability()));
        shareWeatherMessageParts.add("Minimum Temperature: " + this.resources.getString(R.string.temperature_farenheit, daily.getTemperatureMin()));
        shareWeatherMessageParts.add("Maximum Temperature: " + this.resources.getString(R.string.temperature_farenheit, daily.getTemperatureMax()));

        return TextUtils.join("\n", shareWeatherMessageParts);
    }
}
