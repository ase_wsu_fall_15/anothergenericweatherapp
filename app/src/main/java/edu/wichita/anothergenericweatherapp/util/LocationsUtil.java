package edu.wichita.anothergenericweatherapp.util;

import java.util.ArrayList;
import java.util.List;

import edu.wichita.anothergenericweatherapp.models.Location;
import edu.wichita.anothergenericweatherapp.models.RealmLocation;
import edu.wichita.anothergenericweatherapp.models.UserLocation;

/**
 * Created by truongski on 11/30/2015.
 */
public class LocationsUtil {
    public static List<RealmLocation> ToRealmLocations(List<Location> locations) {
        List<RealmLocation> realmLocations = new ArrayList<RealmLocation>();

        for (Location location : locations) {
            realmLocations.add(new RealmLocation(location));
        }

        return realmLocations;
    }

    public static List<Location> ToLocationsFromRealmLocations(List<RealmLocation> realmLocations) {
        List<Location> locations = new ArrayList<Location>();

        for (RealmLocation realmLocation : realmLocations) {
            locations.add(new Location(realmLocation));
        }

        return locations;
    }

    public static List<Location> ToLocationsFromUserLocations(List<UserLocation> userLocations) {
        List<Location> locations = new ArrayList<Location>();

        for (UserLocation userLocation : userLocations) {
            locations.add(new Location(userLocation));
        }

        return locations;
    }
}
