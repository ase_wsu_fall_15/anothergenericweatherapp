package edu.wichita.anothergenericweatherapp.util;

import android.app.AlertDialog;

import edu.wichita.anothergenericweatherapp.dialog.ShareDialog;
import edu.wichita.anothergenericweatherapp.sharing.ShareStrategy;

/**
 * Created by Osama Rao on 03-Dec-15.
 */
public class DialogFactory {

    public static AlertDialog getWeatherShareDialog(ShareStrategy[] shareStrategy, ShareStrategy.ShareArguments shareArguments){
        return new ShareDialog().getShareDialog(shareStrategy, shareArguments);
    }

}
