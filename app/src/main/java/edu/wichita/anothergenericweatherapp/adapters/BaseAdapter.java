package edu.wichita.anothergenericweatherapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseAdapter extends RecyclerView.Adapter<ViewPresenter> {
    private List<Item> entries = new ArrayList<>();

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(final ViewPresenter holder, final int position) {
        holder.bind(entries.get(position).object);
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    protected final void setEntries(List<Item> entries) {
        this.entries.clear();
        this.entries.addAll(entries);
        notifyDataSetChanged();
        // DiffUtil usage here
    }

    @Override
    public int getItemViewType(final int position) {
        return entries.get(position).viewTypeId;
    }

    @Override
    public final ViewPresenter onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return onCreateViewHolder(view, viewType);
    }

    public abstract ViewPresenter onCreateViewHolder(final View view, final int viewType);
}