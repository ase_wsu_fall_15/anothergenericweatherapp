package edu.wichita.anothergenericweatherapp.adapters;

import android.view.View;

public abstract class ViewHolder<T> {

    private final View itemView;

    public ViewHolder(View view) {
        this.itemView = view;
    }

    public abstract void bind(T value);

}
