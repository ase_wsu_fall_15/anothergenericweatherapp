package edu.wichita.anothergenericweatherapp.adapters;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import edu.wichita.anothergenericweatherapp.R;
import edu.wichita.anothergenericweatherapp.models.CityViewHolder;
import edu.wichita.anothergenericweatherapp.models.Location;

public class CitiesViewAdapter extends BaseAdapter {
    private List<Location> locations;

    @Inject
    public CitiesViewAdapter() {
    }

    private void updateEntries() {
        List<Item> items = new ArrayList<>(locations.size());

        if (!locations.isEmpty()) {
            for (Location location : locations) {
                items.add(new Item(R.layout.item_city, location));
            }
        } else {
            items.add(new Item(R.layout.empty, null));
        }
        setEntries(items);
    }

    public void addItem(Location location) {
        locations.add(location);
        notifyItemInserted(locations.size() - 1);
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = new ArrayList<>(locations.size());
        this.locations.addAll(locations);
        updateEntries();
    }

    @Override
    public ViewPresenter onCreateViewHolder(View view, int viewType) {
        if (viewType == R.layout.empty) {
           return new EmptyMessagePresenter(view);
        } else if (viewType == R.layout.item_city){
            return new CityViewHolder(view);
        } else {
            throw new IllegalArgumentException("Unsupported viewtype");
        }
        /*
        View cityView = inflater.inflate(R.layout.item_city, parent, false);
        final CityViewHolder cityViewHolder = new CityViewHolder(cityView);
        cityViewHolder.getAdapterPosition();

        cityView.setOnClickListener(view -> {
            Location tag = locations.get(cityViewHolder.getAdapterPosition());
            Intent intent = new Intent(cityView.getContext(), WeatherActivity.class);
            intent.putExtra("latitude", tag.getLat());
            intent.putExtra("longitude", tag.getLng());
            intent.putExtra("cityName", tag.getCity());
            cityView.getContext().startActivity(intent);
        });


        return cityViewHolder;
        */
    }
}
