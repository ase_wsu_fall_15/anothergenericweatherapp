package edu.wichita.anothergenericweatherapp.adapters;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public abstract class ViewPresenter<T> extends RecyclerView.ViewHolder{

    public ViewPresenter(final View itemView) {
        super(itemView);
    }

    public abstract void bind(T value);
}
