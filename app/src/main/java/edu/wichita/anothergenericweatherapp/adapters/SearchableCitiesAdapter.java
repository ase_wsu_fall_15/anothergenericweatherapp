package edu.wichita.anothergenericweatherapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import edu.wichita.anothergenericweatherapp.R;
import edu.wichita.anothergenericweatherapp.models.Location;

public class SearchableCitiesAdapter extends BaseAdapter implements Filterable {
    private final ArrayList<Location> allLocations = new ArrayList<>();
    private List<Location> activeLocations = Collections.emptyList();
    private LocationFilter locationFilter;

    @Override
    public int getCount() {
        return activeLocations.size();
    }

    @Override
    public Location getItem(int i) {
        return activeLocations.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_auto_text, parent, false);
            convertView.setTag(new CityViewHolder(convertView));
        }
        //noinspection unchecked
        ((ViewHolder) Objects.requireNonNull(convertView).getTag()).bind(activeLocations.get(position));
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (locationFilter == null)
            locationFilter = new LocationFilter();

        return locationFilter;
    }

    public void setCities(final ArrayList<Location> locations) {
        this.activeLocations = locations;
        Collections.sort(this.activeLocations, (that, another) -> that.getCity().compareTo(another.getCity()));
        this.allLocations.clear();
        this.allLocations.addAll(this.activeLocations);
        notifyDataSetChanged();
    }

    private static class CityViewHolder extends ViewHolder<Location> {
        private final TextView txtCityName;

        CityViewHolder(final View view) {
            super(view);
            txtCityName = view.findViewById(R.id.txtCityName);
        }

        @Override
        public void bind(final Location location) {
            txtCityName.setText(String.format("%s %s (%s)", location.getCity(), location.getState(), location.getZip()));
        }
    }

    @SuppressWarnings("unchecked")
    private class LocationFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0) {
                results.values = allLocations;
                results.count = allLocations.size();
            } else {
                ArrayList<Location> filteredLocations = new ArrayList<>();
                Iterator<Location> locationIterator = allLocations.iterator();
                int startOfWindow = 0;
                while (locationIterator.hasNext()) {
                    if (locationIterator.next().getCity().startsWith(constraint.toString())) {
                        break;
                    } else {
                        startOfWindow++;
                    }
                }
                for (int i = startOfWindow; i < allLocations.size(); i++) {
                    if (allLocations.get(i).getCity().startsWith(constraint.toString())) {
                        filteredLocations.add(allLocations.get(i));
                    } else {
                        break;
                    }
                }
                results.values = filteredLocations;
                results.count = filteredLocations.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            activeLocations = (ArrayList<Location>) results.values;
            notifyDataSetChanged();
        }
    }
}