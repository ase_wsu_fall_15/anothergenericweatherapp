package edu.wichita.anothergenericweatherapp.adapters;

import java.util.Objects;

import androidx.annotation.Nullable;

public final class Item {
    public final int viewTypeId;
    @Nullable
    public final Object object;

    public Item(final int viewTypeId, final Object object) {
        this.viewTypeId = viewTypeId;
        this.object = object;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Item item = (Item) o;
        return viewTypeId == item.viewTypeId &&
                Objects.equals(object, item.object);
    }

    @Override
    public int hashCode() {
        return Objects.hash(viewTypeId, object);
    }
}
