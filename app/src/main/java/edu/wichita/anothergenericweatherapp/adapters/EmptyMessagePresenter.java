package edu.wichita.anothergenericweatherapp.adapters;

import android.view.View;

class EmptyMessagePresenter extends ViewPresenter<Void> {

    public EmptyMessagePresenter(final View itemView) {
        super(itemView);
    }

    @Override
    public void bind(final Void value) {

    }
}
