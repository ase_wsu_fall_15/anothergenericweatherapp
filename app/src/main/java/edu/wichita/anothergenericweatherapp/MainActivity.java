package edu.wichita.anothergenericweatherapp;


import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import edu.wichita.anothergenericweatherapp.adapters.CitiesViewAdapter;
import edu.wichita.anothergenericweatherapp.adapters.SearchableCitiesAdapter;
import edu.wichita.anothergenericweatherapp.models.Location;
import edu.wichita.anothergenericweatherapp.services.WeatherBroadcastReceiver;

public class MainActivity extends AppCompatActivity {
    @Inject
    CitiesViewAdapter adapter;
    @Inject
    MainActivityViewModelImpl viewModel;

    private AutoCompleteTextView autoCompleteCityTextView;
    private SearchableCitiesAdapter searchableCitiesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((AnotherGenericWeatherApplication) getApplication()).getAppComponent().inject(this);
        final RecyclerView citiesRecyclerView = findViewById(R.id.cityList);
        ProgressBar loading = findViewById(R.id.loading);
        citiesRecyclerView.setAdapter(adapter);
        searchableCitiesAdapter = new SearchableCitiesAdapter();

        viewModel.setOnLoadingFinishedListener(isLoading -> {
            loading.setVisibility(isLoading ? View.VISIBLE : View.GONE);
            citiesRecyclerView.setVisibility(!isLoading ? View.VISIBLE : View.GONE);
            if (!isLoading) {
                searchableCitiesAdapter.setCities(viewModel.allCities());
                adapter.setLocations(viewModel.savedLocations());
            }
        });

        viewModel.setOnLocationsUpdated(() -> adapter.setLocations(viewModel.savedLocations()));

        createAutoCompleteTextView();

    }

    private void createAutoCompleteTextView() {
        autoCompleteCityTextView = findViewById(R.id.autoCompleteCityTextView);
        autoCompleteCityTextView.setAdapter(searchableCitiesAdapter);
        autoCompleteCityTextView.setThreshold(3);
        autoCompleteCityTextView.setOnItemClickListener((adapterView, view, i, l) -> {
            final Location location = (Location) adapterView.getAdapter().getItem(i);
            autoCompleteCityTextView.setText(location.getCity());
            viewModel.saveLocationToDatabase(location);
            autoCompleteCityTextView.setText("");
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.persistLocations();
    }

    private void configureWeatherNotification(Integer pollingIntervalInMinutes) {
        // Configure AlarmManager
        WeatherBroadcastReceiver weatherBroadcastReceiver = new WeatherBroadcastReceiver();

        weatherBroadcastReceiver.setAlarm(this, pollingIntervalInMinutes);
//        sendBroadcast(new Intent(this, WeatherBroadcastReceiver.class));
    }
}