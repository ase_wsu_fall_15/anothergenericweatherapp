package edu.wichita.anothergenericweatherapp.api;

import edu.wichita.anothergenericweatherapp.models.forecastio.Weather;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

public interface ForecastIO {

    // https://api.forecast.io/forecast/5b45bab9d3b084277da4789f43ffcaa3/23.8267,-122.423
    @GET("https://api.forecast.io/forecast/5b45bab9d3b084277da4789f43ffcaa3/{latitude},{longitude}")
    Call<Weather> getWeatherFromLatLng(
            @Path("latitude") Double latitude,
            @Path("longitude") Double longitude
    );
}
